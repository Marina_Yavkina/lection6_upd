//
//  ImageViewController.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 12/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "ImageViewController.h"
#import "ImageCollectionViewCell.h"
#import "ImageModel.h"
#import <Photos/Photos.h>

NSString * const ImageViewControllerCellRuseIdentifier = @"ImageViewControllerCellRuseIdentifier";

@interface ImageViewController () <UICollectionViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UICollectionView *coolectionView;
@property (nonatomic, strong) ImageModel *model;

@end

@implementation ImageViewController

- (void)loadView {
    self.flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.coolectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.flowLayout];
    self.view = self.coolectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.model = [[ImageModel alloc] init];
    
    self.flowLayout.itemSize = CGSizeMake(100.0f, 100.0f);
    self.flowLayout.minimumLineSpacing = 15.0f;
    self.flowLayout.minimumInteritemSpacing = 15.0f;
    self.flowLayout.sectionInset = UIEdgeInsetsMake(15.0, 15.0f, 15.0f, 15.0f);

    self.coolectionView.backgroundColor = [UIColor whiteColor];
    self.coolectionView.dataSource = self;
    
    [self.coolectionView registerClass:[ImageCollectionViewCell class] forCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(makePhoto:)];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.model sectionsCount];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.model countForSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ImageViewControllerCellRuseIdentifier forIndexPath:indexPath];
    
    id item = [self.model itemForIndexPath:indexPath];

    if ([item isKindOfClass:[PHAsset class]]) {
        PHAsset *asset = (PHAsset *)item;
        [[PHImageManager defaultManager] requestImageForAsset:asset
                                                   targetSize:CGSizeMake(150.0f, 150.f)
                                                  contentMode:PHImageContentModeDefault
                                                      options:nil
                                                resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                                                    cell.imageView.image = result;
                                                }];
    }
    
    if ([item isKindOfClass:[NSString class]]) {
        NSString *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *lastPathComponent = item;
        cell.imageView.image = [UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:lastPathComponent]];
    }
    
    //cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld",(long)indexPath.item]];
    return cell;
}

- (void)makePhoto:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypeCamera;
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    }
    else {
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0) {
    [self.model saveImage:image];
    [self.coolectionView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
