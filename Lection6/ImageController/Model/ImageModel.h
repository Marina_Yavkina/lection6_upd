//
//  ImageModel.h
//  Lection6
//
//  Created by Vladislav Grigoriev on 12/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageModel : NSObject

- (NSInteger)sectionsCount;

- (NSInteger)countForSection:(NSInteger)section;

- (id)itemForIndexPath:(NSIndexPath *)indexPath;

- (void)saveImage:(UIImage *)image;

@end
