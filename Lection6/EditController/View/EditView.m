
//
//  EditView.m
//  Lection6
//
//  Created by Vladislav Grigoriev on 07/11/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "EditView.h"

@implementation EditView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        _iconButton = [[UIButton alloc] init];
        [_iconButton setImage:[UIImage imageNamed:@"addIcom"] forState:UIControlStateNormal];
        [self addSubview:_iconButton];
        
        _titleTextField = [[UITextField alloc] init];
        _titleTextField.borderStyle = UITextBorderStyleRoundedRect;
        [self addSubview:_titleTextField];
        
        _detailTextView = [[UITextView alloc] init];
        [self addSubview:_detailTextView];
        
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    
    CGFloat offset = 10.0f;
    self.iconButton.frame = CGRectMake(offset, offset + self.topOffset, 100.0f, 100.0f);
    
    self.titleTextField.frame = CGRectMake(self.iconButton.frame.origin.x + self.iconButton.bounds.size.width + offset, self.topOffset +  offset + (self.iconButton.bounds.size.height - 50.0f) / 2.0f, self.bounds.size.width - self.iconButton.frame.origin.x - self.iconButton.bounds.size.width - offset, 50.0f);
    
    self.detailTextView.frame = CGRectMake(offset,
                                           CGRectGetMaxY(self.iconButton.frame), self.bounds.size.width - offset * 2, 50);
}

- (void)setTopOffset:(CGFloat)topOffset {
    if (_topOffset != topOffset) {
        _topOffset = topOffset;
        [self setNeedsLayout];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
